

% Guiao 6

DualTone = [  0  941  1336;
              1  697  1209;
              2  697  1336;
              3  697  1477;
              4  770  1209;
              5  770  1336;
              6  770  1477;
              7  852  1209;
              8  852  1336;
              9  852  1477;
                             ];

SEQ = [1 5 7 3];
dur_sep = 50e-3; % intervalo de 50 ms
dur_dig = 100e-3 % tempo do d�gito
fa = 8e3;
ta = 1/fa
duracaoSinal = 150e-3 * length(SEQ);
N_sep = dur_sep * fa;
N_dig = dur_dig * fa;

COD_SEP = zeros(1,N_sep); % criar uma linha de zeros do tamanho das amostras de separa��o
COD_DIG = zeros(length(SEQ),N_dig); % cria matrizes com zeros do tamanho length(SEQ) * N_dig
COD_SEQ = [];     % sequencia completa do sinal.


% 2 sinais => amplt de pic a pic de cada = 1 => amplitude de cada 0.5
Ns = 2; % n�mero de sinusoides a somar
maxA = 2;
A = maxA/(Ns*2);
n = 0:N_dig-1;
COD_SEQ = [];     % sequencia completa do sinal.

for i=1:length(SEQ)
    I = find(DualTone(:,1) == SEQ(i)); % devolve o numero da linha onde se encontra o d�gito de SEQ
    COD_DIG(i,:) = A * sin(2*pi*DualTone(I,2)*n/fa) + A * sin(2*pi*DualTone(I,3)*n/fa);
    COD_SEQ = [COD_SEQ COD_DIG(i,:) COD_SEP];
    figure;
    stem(n,COD_DIG(i,:));title(sprintf('%d : %d hz %d hz',SEQ(i),DualTone(I,2),DualTone(I,3)));
end;

checkFrequency(COD_SEQ,fa,'',4,[500 1700 0 400]);

% Exercicio 1.b)
    %Adi��o de Ruido

ruido = repmat([-0.5 0.5],1,length(COD_SEQ)/2);
COD_ruido = COD_SEQ + ruido;
checkFrequency(COD_ruido,fa,'',4,[500 1700 0 400]);

snr = SNR(COD_ruido,COD_SEQ);

% Exercicio 2

x = load('dtmf1.txt');
N = length(x);
fa = 8e3;
ta = 1/fa;
dur_sinal = N * ta;

% Exercicio 2.a)
[ X Y ] = checkFrequency(x,fa,'mod',4,0);
dur_dig = abs((X(2)-X(1)))*ta; % aproximadamente 0.10 ms
dur_sep = abs((X(4)-X(3)))*ta; % aproximadamente 0.05 ms

% Exercicio 2.b)

for i=0:3
    digit(i+1,:) = checkFrequency(x(1+1000*i:1000+1000*i),fa,'mod',2,[650 1650 0 200]);
    pause
end;

    %Resultado
    %   944 1336 => 0
    %   768 1207 => 4
    %   848 1207 => 7
    %   695 1207 => 1

% Exercicio 2.c
xf = retirarRuido(x,fa,1633,697,'dentro');
[ X Y ] = checkFrequency(xf,fa,'mod',0,0);






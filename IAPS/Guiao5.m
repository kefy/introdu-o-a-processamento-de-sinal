%******************Guiao N 5%******************************

%PART 1

f1 = 15;
f2 = 40;
duracao = 4;
N = 1000;
fa = N/duracao;
ta = 1/fa
n = 0:N-1;
x1 = cos(2*pi*f1*n/fa);
x2 = cos(2*pi*f2*n/fa);
x = x1 + x2;

% Confirma��o das Frequencias.

[X Y] = checkFrequency(x,fa,'mod',2,[0 fa/2 -750 750]); % 2 recolha de dados do gr�fico e visualizar frequ�ncias.


%PART 2

load('ecgNoise.txt');
N = length(ecgNoise)
fa = 250;
ta = 1/fa;

% Exercicio 1.a)
    % Resposta: A primeira risca do gr�fico corresponde ao valor m�dio do sinal
[X Y] = checkFrequency(ecgNoise,fa,'mod',1,0);
valor_medio = mean(ecgNoise) * N;

% Exercicio 1.b)

    y = fft(ecgNoise);
    y(1) = 0;
    newEcgNoise = ifft(y);
    [X Y] = checkFrequency(newEcgNoise,fa,'mod',0,0);

% Exercicio 1.c)
load('ecg1.txt');

[X Y] = checkFrequency(ecgNoise,fa,'mod',2,[0 fa/2 -60 60]);
[X1 Y1] = checkFrequency(ecg1,fa,'mod',0,0);
   %Resposta: 2 componentes sinusoidais
   
% Exercicio 1.d)   
[X Y] = checkFrequency(ecgNoise,fa,'mod',2,[0 fa/2 -60 60]);
[X1 Y1] = checkFrequency(ecg1,fa,'mod',0,0);
   %Resposta: Frequencia de 15 e 16 hz.
   
% Exercicio 1.e)
newEcgNoise = retirarRuido(newEcgNoise,fa,X(1),X(2),'fora'); % Retira ruido dentro do limite inferior do ruido X(1) e limite Superior do ruido X(2)
[Xa Yb] = checkFrequency(newEcgNoise,fa,'mod',0,[0 fa/2 0 60]);


% Exercicio 1.f
    % Calcular o SNR do sinal distorcido e o sinal filtrado
        snr = SNR(ecgNoise,newEcgNoise,ta);
        
% 2. ECG1
    load('ecg1.txt');
    fa = 250;
    ta = 1/fa
%Exercicio 2.a
    dur = length(ecg1) * ta;
    t = 0:ta:dur-ta
    figure
    plot(t,ecg1);
    [X Y ] = ginput(2);
    [X Y]= checkFrequency(ecg1,fa,'mod',2,[0 fa/2 -60 60]);
 %Exercicio 2.b
    [X Y]= checkFrequency(ecg1,fa,'mod',1,[0 fa/2 -60 60]);
    % Resposta: A largura de banda � 45 hz porque dai para a frente o sinal
    % nao contem energia relevante para a m�dia do sinal
 %Exercicio 2.c
    %Resposta: Como a banda do sinal � de 45 hz a frequencia de amostragem
    %minima pode ser 45 hz um avez que fmax = 90 hz dai ser possivel usar
    %fa < 250
    
 
    
function newX = retirarRuido( x, fa, limiteInferior, limiteSuperior,mode )
%RETIRARRUIDO Fun��o para retirar ruido entre intervalos
% Usada no exercicio 1.e do guiao 5
% 'dentro' - Se a parte que interessa estiver dentro coloca o que estiver fora do
% 'fora'   - Se a parte que interessa estiver fora do intervalo coloca dentro a 0

    y = fft(x);
    df = fa/length(x);
    n  = 0:length(x)-1;
    f  = df * n;
    if(strcmp(mode,'dentro'))
         y(limiteInferior < f | limiteSuperior > f) = 0;
         newX = ifft(y);
    else
         y(limiteInferior < f & limiteSuperior > f) = 0;
         newX = ifft(y);
    end;
   
end


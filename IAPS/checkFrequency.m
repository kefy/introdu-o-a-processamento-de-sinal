function [ X Y ] = checkFrequency( x , fa, mode, input, ax )
%CHECKFREQUENCY Verifica��o das frequencias de um sinal.
%   MODE
%        -'mod' - Apresenta gr�fico do sinal no dominio do tempo e do m�dulo no
%        dominio da frequencia
%        -'arg' - Apresenta gr�fico do sinal no dom�nio do tempo e do argumento
%        no dom�nioda frequencia.
%        -qualquer coisa nao apresenta nada
%   input
%       Devolve o numero de inputs que colocar como argumento. Se for 0
%       devolve [ 0 0];
%
%   ###############IMPORTANTE##########################
%   ALTERAR MANUALMENTE OS EIXOS PARA O GR�FICO DO DOM�NIO DO TEMPO.
% Esta fun��o serve para confirmar as frequencias de um sinal.
% a fun��o faz uso da fft para obter o sinal em frequencia.

df = fa/length(x);
n  = 0:length(x)-1;
f  = df * n;
y = fft(x);

figure
subplot(3,1,1),stem(n,x),xlabel('n'),ylabel('x(n)'),title('Gr�fico no dom�nio de tempo(amostras)'),

if (strcmp(mode,'mod'))
    Mod_x = abs(y);
    subplot(2,1,1),stem(n,x),xlabel('n'),ylabel('x(n)'),title('Gr�fico no dom�nio de tempo(amostras)');
    if (length(ax) ~= 1)
        subplot(2,1,2),stem(f,Mod_x),xlabel('f(Hz)'),ylabel('Mod_x'),title('Gr�fico do m�dulo de X'),axis(ax);
    else
        subplot(2,1,2),stem(f,Mod_x),xlabel('f(Hz)'),ylabel('Mod_x'),title('Gr�fico do m�dulo de X');
    end;
    if (input ~= 0)
         [X Y] = ginput(input);
    else
         X = 0;
         Y = 0;
    end;
    
elseif (strcmp(mode,'arg'))
    arg_x = angle(y);
    subplot(2,1,1),stem(n,x),xlabel('n'),ylabel('x(n)'),title('Gr�fico no dom�nio de tempo(amostras)'),
    subplot(2,1,2),stem(f,arg_x),xlabel('f(Hz)'),ylabel('Arg_x'),title('Gr�gico do argumento de X');
else
    Mod_x = abs(y);
    arg_x = angle(y);
    subplot(3,1,1),stem(n,x),xlabel('n'),ylabel('x(n)'),title('Gr�fico no dom�nio de tempo(amostras)'),
    if (length(ax) ~= 1)
        subplot(3,1,2),stem(f,Mod_x),xlabel('f(Hz)'),ylabel('Mod_x'),title('Gr�fico do m�dulo de X'),axis(ax);
    else
        subplot(3,1,2),stem(f,Mod_x),xlabel('f(Hz)'),ylabel('Mod_x'),title('Gr�fico do m�dulo de X');
    end;
    subplot(3,1,3),stem(f,arg_x),xlabel('f(Hz)'),ylabel('Arg_x'),title('Gr�gico do argumento de X');
    if (input ~= 0)
         [X Y] = ginput(input);
    else
         X = 0;
         Y = 0;
    end;
end;


    


end

